﻿using AvitoProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvitoProject.Repository
{
    public interface IDataManipulate
    {
        public ICollection<AdInfo> GetSortedAds(bool desc);
        public ICollection<AdInfo> GetFilteredAds(Func<AdInfo> filter);
    }
}
