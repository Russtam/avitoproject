﻿using AvitoProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvitoProject.Repository
{
    public interface IAdsRepository
    {
        public IQueryable<AdInfo> GetAllAds();
        public IQueryable<AdInfo> GetAdsOfProfile(Profile profile);
        public void CreateNewAd(AdInfo ad);
        public void DeleteAd(Guid guid);
        public Task CreateNewAdAsync(AdInfo ad);
        public AdInfo ChangeAd(Guid guid);
        public AdInfo SelectAd(int guid);
        public AdInfo GetAd();
    }
}
