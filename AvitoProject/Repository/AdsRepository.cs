﻿using AvitoProject.Infrasrtucture;
using AvitoProject.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AvitoProject.Repository
{
    public class AdsRepository : IAdsRepository
    {
        AdDbContext _context;
        readonly IDataManipulate _dataManipulate;
        public AdsRepository(AdDbContext context, IDataManipulate dataManipulate)
        {
            _context = context;
            _dataManipulate = dataManipulate;
        }
        public AdInfo ChangeAd(Guid guid)
        {
            throw new NotImplementedException();
        }

        public void CreateNewAd(AdInfo ad)
        {
            ad.Id = Guid.NewGuid();
            ad.CreationDate = DateTime.Now;
            ad.ChangeDate = DateTime.Now;
            ad.AdStatus = AdStatus.Open;
            _context.AddAsync(ad);
            _context.SaveChanges();
        }

        public void DeleteAd(Guid guid)
        {
            throw new NotImplementedException();
        }

        public IQueryable<AdInfo> GetAdsOfProfile(Profile profile)
        {
            throw new NotImplementedException();
        }
        public IQueryable<AdInfo> GetAllAds() => _context.AdInfo;

        public AdInfo SelectAd(int guid)
        {
            throw new NotImplementedException();
        }

        public async Task CreateNewAdAsync(AdInfo ad)
        {
            await Task.Run(() => CreateNewAd(ad));
        }
        public AdInfo GetAd() => new AdInfo
        {
            AdStatus = AdStatus.Open,
            CreationDate = DateTime.Now,
            ChangeDate = DateTime.Now, 
        };
    }
}
