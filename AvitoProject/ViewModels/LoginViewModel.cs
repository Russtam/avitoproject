﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AvitoProject.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage ="Не указан email")]
        public string Email { get; set; }
        [Required(ErrorMessage ="Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set;}

        public string ReturnUrl { get; set; }
    }
}
