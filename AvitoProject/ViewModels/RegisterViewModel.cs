﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AvitoProject.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name="Адрес электронной почты")]
        public string Email { get; set; }
        [Required]
        [Display(Name="Имя")]
        [RegularExpression(@"\S+")]
        public string Name { get; set; }
        [Required]
        [Display(Name="Номер телефона")]
        public string Phone { get; set; }
        public string City { get; set; }
        [Required]
        [Display(Name ="Пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [Compare("Password", ErrorMessage ="Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name="Введите пароль повторно")]
        public string PassConfirm { get; set; }
    }
}
