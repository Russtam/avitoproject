﻿using AvitoProject.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AvitoProject.Infrasrtucture
{
    public class AdDbContext : IdentityDbContext
    {
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<AdInfo> AdInfo { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<AdHistory> Histories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
        public AdDbContext(DbContextOptions<AdDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Profile>().HasIndex(profile => profile.Email);
            builder.Entity<Profile>().Property("Email").IsRequired();
            base.OnModelCreating(builder);
        }

    }
}
