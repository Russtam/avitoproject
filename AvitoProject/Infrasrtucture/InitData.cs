﻿using AvitoProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvitoProject.Infrasrtucture
{
    public static class InitData
    {
        public static void Initialize(AdDbContext context)
        {

            if (!context.Categories.Any())
            {
                context.Categories.AddRange(
                    new Category
                    {
                        Name = "Autos"
                    },
                    new Category
                    {
                        Name = "Animals",
                    },
                    new Category
                    {
                        Name = "Computers"
                    }
                    );
                context.AddRange(
                    new SubjectType
                    {
                        CategoryId = 2,
                        Name = "Dogs"
                    },
                    new SubjectType
                    {
                        CategoryId = 2,
                        Name = "Cats"
                    },
                    new SubjectType
                    {
                        CategoryId = 2,
                        Name = "Birds"
                    }
                    );
                context.AddRange(
                    new SubjectType
                    {
                        CategoryId = 1,
                        Name = "Huyndai"
                    },
                    new SubjectType
                    {
                        CategoryId = 1,
                        Name = "Mersedes-Bens"
                    },
                    new SubjectType
                    {
                        CategoryId = 1,
                        Name = "Audi"
                    }
                    );
                //context.Profiles.AddRange
                //    (
                //    new Profile
                //    {
                //        Name = "Nivkov Anton",
                //        City = "Moscow",
                //        Email = "nivkAn@mail.ru",
                //        Phone = "+79854641445"
                //    },
                //    new Profile
                //    {
                //        Name = "Pupkin Oleg",
                //        City = "Yekaterinburg",
                //        Email = "pupok@mail.ru",
                //        Phone = "+79857451214"
                //    },
                //    new Profile
                //    {
                //        Name = "Avenkov Vasiliy",
                //        City = "Ryazan",
                //        Email = "avvas@mail.ru",
                //        Phone = "+79024788975"
                //    },
                //    new Profile
                //    {
                //        Name = "Petrov Petr",
                //        City = "Moscow",
                //        Email = "petrI@mail.ru",
                //        Phone = "+79320015066"
                //    }
                //    );
                context.SaveChanges();
            }
        }
    }
}
