﻿using System;
using System.Collections.Generic;

namespace AvitoProject.Models
{
    public class AdInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int SubjectTypeId { get; set; }
        public Profile Profile { get; set; }
        public decimal Price { get; set; }
        public AdStatus AdStatus { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ChangeDate { get; set; }
        public List<AdHistory> History { get; set; }
    }
}
