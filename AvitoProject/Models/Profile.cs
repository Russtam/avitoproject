﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AvitoProject.Models
{
    public class Profile : IdentityUser
    {
        public string City { get; set; }
        public List<AdInfo> Products { get; set; }


        public override string ToString()
        {
            return $"Name: {UserName}\nPhone: {PhoneNumber}\nMail: {Email}";
        }
    }
}
