﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvitoProject.Models
{
    public class AdHistory
    {
        public int Id { get; set; }
        public Guid AdInfoId { get; set; }
        public DateTime? ChangeDate { get; set; }
        public int Status { get; set; }
        public string Comment { get; set; }
    }
}
