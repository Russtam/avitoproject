﻿

namespace AvitoProject.Models
{
    public enum AdStatus
    {
        Open = 1,
        Close = 2,
        Cancel = 3,
    }
}
