﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AvitoProject.Models;
using AvitoProject.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using AvitoProject.Infrasrtucture;

namespace AvitoProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAdsRepository _adsReposiroty;
        private readonly ILogger<HomeController> _logger;
        private readonly AdDbContext _dbContext;

        public HomeController(ILogger<HomeController> logger, IAdsRepository adsRepository, AdDbContext context)
        {
            _dbContext = context;
            _logger = logger;
            _adsReposiroty = adsRepository;
        }
        public async Task<IActionResult> Index()
        {
            var ads = _adsReposiroty.GetAllAds();
            return View(await ads.ToListAsync());
        }
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create()
        {
            if (_adsReposiroty != null)
            {
                var ad = await Task.Run(() => _adsReposiroty.GetAd());
                return View(ad);
            }
            else
                throw new ArgumentNullException();
        }

        [HttpPost]
        public async Task<IActionResult> Create(AdInfo ad)
        {
            if (ModelState.IsValid)
            {
                await _adsReposiroty.CreateNewAdAsync(ad);
                return RedirectToAction(nameof(Index));
            }
            return View(ad);
        }


        public IActionResult SignIn()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
