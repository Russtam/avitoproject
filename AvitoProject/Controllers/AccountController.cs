﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AvitoProject.Infrasrtucture;
using AvitoProject.Models;
using AvitoProject.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AvitoProject.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<Profile> _userManager;
        private readonly SignInManager<Profile> _signInManager;
        private readonly AdDbContext _context;

        public AccountController(UserManager<Profile> userManager, SignInManager<Profile> signInManager, AdDbContext context)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel registerModel)
        {
            if (ModelState.IsValid)
            {
                var profile = new Profile
                {
                    City = registerModel.City,
                    Email = registerModel.Email,
                    PhoneNumber = registerModel.Phone,
                    UserName = registerModel.Name
                };
                var createResult = await _userManager.CreateAsync(profile, registerModel.Password);
                if (createResult.Succeeded)
                {
                    await _signInManager.SignInAsync(profile, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in createResult.Errors)
                        ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(registerModel);
        }
       
        public IActionResult Login()
        {
            return View();
        }




        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {

            if (ModelState.IsValid)
            {
                var username = string.Empty;
                if (Regex.IsMatch(model.Email, @".+@.+\.(ru|com|рф)"))
                {
                    var user = await _userManager.FindByEmailAsync(model.Email);
                    username = user == null ? "" : user.UserName;
                }
                else
                    username = model.Email;
                if (string.IsNullOrEmpty(username))
                {
                    ModelState.AddModelError("", "Данный пользователь не зарегистрирован в системе");
                    return View(model);
                }

                var result = await _signInManager.PasswordSignInAsync(username, model.Password, false, false);
                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }


    }
}